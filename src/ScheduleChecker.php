<?php

namespace Jobby;

use Cron\CronExpression;

class ScheduleChecker
{
    protected $timezone;

    /**
     * @param null|string $timezone
     */
    public function __construct($timezone = null)
    {
        $this->timezone = $timezone;
    }

    /**
     * @param string|callable $schedule
     * @return bool
     */
    public function isDue($schedule)
    {
        if (is_callable($schedule)) {
            return call_user_func($schedule);
        }

        $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $schedule);
        if ($dateTime !== false) {
            return $dateTime->format('Y-m-d H:i') == (date('Y-m-d H:i'));
        }

        return CronExpression::factory((string)$schedule)->isDue('now', $this->timezone);
    }
}
